# `await_macros`

UPDATE (2019-05-09): The crate now expands on nightly! Be sure you have at least the 2019-05-08 nightly to use the crate. It will error on stable, so uh, don't try to use it on stable yet :)

A bunch of macros that literally only expand to `(expr).await` as an alternative to the postfix syntax. Have fun!

Current list of macro names are:
```rust
avait!(future)

avvait!(future)

back_to_the!(future)

divine!(future)

avast!(future)

anticipate!(future)

hope!(future)

foresee!(future)

envision!(future)

doubt!(future)

predict!(future)

be_afraid!(future)

count_on!(future)

prophesize!(future)

foretell!(future)

wait_up!(future)

hey_give_that_back!(future)

aweight!(future)

hold_up!(future)

presume!(future)

pls_gib!(future)

now_becomes_the!(future)

ayywait!(future)

r#await!(future)

predestined!(future)

kismet!(future)

godot!(future)

wait!(future)

aqait!(future)
```

## Example

```rust
use await_macros::back_to_the;

fn main() {
    let future = get_some_future();

    let result = back_to_the!(future);
}
```

## License

`await_macros` is licensed under both MIT and Apache 2.0