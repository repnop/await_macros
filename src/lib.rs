//! Macros that expand to `(expr).await`, its as simple as that.
//! 
//! Requires nightly Rust, 2019-05-08 or greater.

macro_rules! gen_macro {
    ($($i:ident,)+) => {
        $(
            #[macro_export]
            macro_rules! $i {
                ($e:expr) => {
                    $e.await
                }
            }
        )+
    };
}

gen_macro! {
    avait,
    avvait,
    back_to_the,
    divine,
    avast,
    anticipate,
    hope,
    foresee,
    envision,
    doubt,
    predict,
    be_afraid,
    count_on,
    prophesize,
    foretell,
    wait_up,
    hey_give_that_back,
    aweight,
    hold_up,
    presume,
    pls_gib,
    now_becomes_the,
    ayywait,
    r#await,
    predestined,
    kismet,
    godot,
    wait,
    aqait,
}